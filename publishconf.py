#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

AUTHOR = 'WCDP2016'
SITENAME = '2016 Workshop on Collaborative Data Projects'
SITEURL = 'http://odw.tw/2016'
RELATIVE_URLS = False

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None

PATH = 'content'

TIMEZONE = 'Asia/Taipei'

DEFAULT_LANG = 'en'

LOCALE = 'en_US.utf8'

DELETE_OUTPUT_DIRECTORY = True

DEFAULT_PAGINATION = 1

PLUGIN_PATHS = ['plugins']

PLUGINS = ['i18n_subsites']

I18N_SUBSITES = {
    'zh-tw': {
        'SITENAME': '2016 資料協作專案工作坊',
        'LOCALE': 'zh_TW.utf8',
        'THEME_STATIC_DIR': 'theme',
        'STATIC_PATHS': ['images'],
        'MENUITEMS': [('議程', './programme.html'), ('講者', './speaker.html'), ('地點', './location.html'), ('報名', './registration.html'), ('English', '../')]
        }
    }

THEME = 'themes/gum'

MENUITEMS = [('Programme', './programme.html'), ('Speaker', './speaker.html'), ('Location', './location.html'), ('Registration', './registration.html'), ('中文', './zh-tw')]

STATIC_PATHS = ['images']
