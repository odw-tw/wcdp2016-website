Title: Assessing Value of Biomedical Digital Repositories
Slug: assessing-value-of-biomedical-digital-repositories
Lang: en
Date: 2016-11-11
Category: Programme

**Chun-Nan Hsu**

School of Medicine, UC San Diego

**Abstract**

Digital repositories bring direct impacts and influence to the research community and society but at the moment it is challenging to objectively measure their value. We distinguished the difference between impacts and influence and discussed measures and mentions as the basis of a quality metric of a digital repository. It is challenging to define a single perfect metric that covers all quality aspects. We argue that these challenges may potentially be overcome through the introduction of standard resource identification and data citation practices. Full implementation of these standards will depend on cooperation from all stakeholders --- digital repositories, authors, publishers, and funding agencies, for which we have been gaining support with endorsements and resource investments.
