Title: Speaker
Slug: speaker
Lang: en
Date: 2016-11-11

<a name="te-en-lin"></a>
#### [Te-En Lin] [1]

Taiwan Endemic Species Research Institute

![Te-En Lin] [p1]

***

<a name="yu-huang-wang"></a>
#### Yu-Huang Wang

Research Center for Biodiversity, Academia Sinica

![Yu-Huang Wang] [p2]

***

<a name="guan-shuo-mai"></a>
#### Guan-Shuo Mai

Research Center for Biodiversity, Academia Sinica

***

<a name="ling-jyh-chen"></a>
#### [Ling-Jyh Chen] [3]

Institute of Information Science, Academia Sinica

![Ling-Jyh Chen] [p3]

***

<a name="wuulong-hsu"></a>
#### Wuulong Hsu

LASS founder

***

<a name="tyng-ruey-chuang"></a>
#### [Tyng-Ruey Chuang] [4]

Institute of Information Science, Academia Sinica

![Tyng-Ruey Chuang] [p4]

***

<a name="ruoh-rong-yu"></a>
#### [Ruoh-Rong Yu] [5]

Center for Survey Research, Research Center for Humanities and Social Sciences, Academia Sinica

![Ruoh-Rong Yu] [p5]

***

<a name="mélanie-dulong-de-rosnay"></a>
#### [Mélanie Dulong de Rosnay] [6]

CNRS (Centre National de la Recherche Scientifique)

![Mélanie Dulong de Rosnay] [p6]

***

<a name="chun-nan-hsu"></a>
#### [Chun-Nan Hsu] [7]

School of Medicine, UC San Diego

![Chun-Nan Hsu] [p7]

***

<a name="mike-linksvayer"></a>
#### [Mike Linksvayer] [8]

Independent Scholar

![Mike Linksvayer] [p8]

Mike Linksvayer serves on the board of Software Freedom Conservancy. From 2003 to 2012 he served as CTO and VP of Creative Commons, where he is now a Senior Fellow. In 2000 he co-founded Bitzi, an early open content/open data mass collaboration platform.

[1]: http://tesri.tesri.gov.tw/research/researcher/z_09.htm
[3]: https://sites.google.com/site/cclljj/
[4]: http://www.iis.sinica.edu.tw/~trc/public/
[5]: http://www.rchss.sinica.edu.tw/people/bio.php?PID=51
[6]: http://www.iscc.cnrs.fr/spip.php?article1558
[7]: https://healthsciences.ucsd.edu/som/dbmi/people/faculty/Pages/chun-nan-hsu.aspx
[8]: http://gondwanaland.com/mlog/

[p1]: ./images/te-en-lin.jpg
[p2]: ./images/yu-huang-wang.jpg
[p3]: ./images/ling-jyh-chen.jpg
[p4]: ./images/tyng-ruey-chuang.jpg
[p5]: ./images/ruoh-rong-yu.jpg
[p6]: ./images/mélanie-dulong-de-rosnay.jpg
[p7]: ./images/chun-nan-hsu.jpg
[p8]: ./images/mike-linksvayer.jpg
