Title: Citizen Science and Collaborative Data Crosswalks
Slug: citizen-science-and-collaborative-data-crosswalks
Lang: en
Date: 2016-11-15
Category: Programme

**Mike Linksvayer**

Independent Scholar

**Abstract**

What is the impact and influence of citizen science? It isn't captured quantitatively by calculating impact factors or counting patents. Qualitatively, it lacks the prestige of professional science. This talk will outline areas for "crosswalk" initiatives that could further both understanding and positive impact of science and data, whatever their contexts, and more fruitful collaboration across contexts, including literal "data crosswalks" and the role of wikidata, and institutional/policy opportunities to exploit collaborative data about citizen science.
