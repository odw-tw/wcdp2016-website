地點
####
:slug: location
:lang: zh-tw
:date: 2016-11-11

**地點：中央研究院 資訊科學研究所 新館 106 演講廳**

**地址：台北市南港區研究院路二段 128 號**

.. raw:: html

   <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />
   <script src="https://unpkg.com/leaflet@1.0.1/dist/leaflet.js"></script>
   <div id="mapid" style="height: 300px;"></div>
   <script>
     var map = L.map('mapid').setView([25.04114599194039, 121.6146183013916], 18);

     L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
       maxZoom: 18,
       attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
           '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
	   'Imagery © <a href="http://mapbox.com">Mapbox</a>',
       id: 'mapbox.streets'
   }).addTo(map);

     L.marker([25.04114599194039, 121.6146183013916]).addTo(map)
         .bindPopup('中央研究院 資訊科學研究所')
         .openPopup();

   </script>

**中央研究院院區地圖**

.. image:: ../images/sinicamap_e.gif
   :alt: 中央研究院院區地圖

資訊所為圖中「紫色 32 號」。
